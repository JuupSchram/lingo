console.log("Lingo started");

var debug = false; // Change to true for debugging, it will show usefull stuff in the console

// Do not eddit this block, it defines important variables etc.
var line = 0;
var selectedWord = "error";
var wordArr = [];
var wordArrSorted = [];

// Select random word from wordList and asign it to selectedWord
var randIndex = Math.floor(Math.random() * wordsList.length);
var selectedWord = wordsList[randIndex];

// Split the selected word in separate letters and put it in array
wordArr = selectedWord.split("");

// If debug enabled it will print the selected word in chat
if (debug) {
	console.log("The word is: " + selectedWord);
}

// Functions:
function compareChar() { // This function will compare the input chars with the selected word chars and asign the correct class to the input field
	for (var n = 0; n < 5; n++) {
		if (wordArr[n] === inputArr[n]) {
			inputArr[n] = true;
			wordArr[n] = false;
			char = document.getElementById("letter" + line + "_" + n);
			char.setAttribute("class", "correctPlace");
		}
	}
	for (var n = 0; n < 5; n++) {
		var found = false;
		for (var o = 0;
			(o < 5) && (found === false); o++) {
			if (wordArr[n] === inputArr[o]) {
				inputArr[o] = false;
				found = true;
				char = document.getElementById("letter" + line + "_" + o);
				char.setAttribute("class", "correct");
			}
		}
	}
	if (debug) {
		console.log("inputArr = " + inputArr);
		console.log("wordArr = " + wordArr);
	}
}

function setWritingLine() { // Disables all lines except the one that the player should enter their guessed word in
	for (var l = 0; l < 5; l++) {
		for (var m = 0; m < 5; m++) {
			if (l === line) {
				input = document.getElementById("letter" + l + "_" + m);
				input.disabled = false;
			} else {
				input = document.getElementById("letter" + l + "_" + m);
				input.disabled = true;
			}
		}
	}
}

function restartButton() { // Function that gets ran when the "restart" button is pressed, this wil restart the game (refresh)
	location.reload();
}

function checkButton() { // Function that gets ran when the "enter" button is pressed, this will check if there is any empty fields, run compareChar() and increase line value etc.
	var word = "";
	var hasEmptyChar = false;
	for (var k = 0; k < 5; k++) {
		char = document.getElementById("letter" + line + "_" + k).value;
		if (char == "") {
			hasEmptyChar = true;
		} else {
			word += char;
		}
	}
	word = word.toLowerCase();
	inputArr = word.split("");
	if (hasEmptyChar) {
		alert("Line contains an empty field!");
		if (debug) {
			console.log("Line " + line + " contains empty input");
		}
	} else {
		if (debug) {
			console.log("Word on line " + line + " is " + word)
		}
		compareChar();
		line++;
		setWritingLine()
		if (line > 4) {  // LOSE
			var par = document.createElement("p");
			var text = document.createTextNode("LOSE! The word was: " + selectedWord);
			par.setAttribute("class", "revealWordLose");

			par.appendChild(text);
			var element = document.getElementById("revealWord");
			element.appendChild(par);

			var button = document.getElementById("eButton");
			button.setAttribute("onClick", ""); 
		}
		if (word === selectedWord) { // WIN
			var par = document.createElement("p");
			var text = document.createTextNode("WIN! The word was: " + selectedWord);
			par.setAttribute("class", "revealWordWin");

			par.appendChild(text);
			var element = document.getElementById("revealWord");
			element.appendChild(par);

			var button = document.getElementById("eButton");
			button.setAttribute("onClick", ""); 
		}
		wordArr = selectedWord.split("");
	}
}

// Create the playfield
for (var i = 0; i < 5; i++) {
	var r = document.getElementById("table");
	var tr = document.createElement("tr");

	for (var j = 0; j < 5; j++) {
		var bg = document.createElement("div");
		bg.setAttribute("class","bg");
		var input = document.createElement("input");
		if (("letter" + i + "_" + j) === "letter0_0") {
			input.setAttribute("value", wordArr[0]);
		}
		input.setAttribute("id", "letter" + i + "_" + j);
		input.setAttribute("type", "text");
		input.setAttribute("maxLength", 1);
		var td = document.createElement("td");
		td.appendChild(bg);
		bg.appendChild(input);
		tr.appendChild(td);
	}

	r.appendChild(tr);

}

// See function "setWritingLine()"
setWritingLine();